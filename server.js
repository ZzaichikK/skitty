// var http = require('http');
// http.createServer(function (req, res) {
//   res.writeHead(200, {'Content-Type': 'text/html'});
//   res.end('s\'Kitty is running!');
// }).listen(process.env.VMC_APP_PORT || 1337, null);


/****************************
 *
 *   Actual Robot Code
 *
 ****************************/

// Load Modules
var Bot = require('ttapi');
var OAuth = require('oauth').OAuth;
// var StatTracker = require("./node_modules/statTracker.js").StatTracker;

// turntable Auth
var AUTH = 'auth+live+1b712d8d9b8fc52147add879ac69d1efe1605162';
var USERID = '50124cddaaa5cd670f0004de';

// twitter auth
var consumer_key = 'ObDjfJOXTqcB2Rec9szbsw';
var consumer_secret = 'WJ6WamdyCyAakILtpKvWRl2kG7zGQTdV5WJGSwc3o08';
var access_token = '611466212-Q8vq9d8PZqOQeeYE2qb7q05G0JP4GO3ruXpHDIfs';
var access_token_secret = 'wzXuUbyUt6qpvdWGOHfe2Jy80trIx5NV5agKAOHRk';

// room variables
var ROOMID = '4ded3b7e99968e1d29000047';
var roomRules = "NO HEAVY WUBS/Brostep, NO Metal, NO Country, NO House/Techno, NO Song Limits, No Queues, No Spam Please check the Room Rules for More Details: http://tinyurl.com/codingSoundtrack";
var djRules = "DJ's should awesome everything - if you're at work (like most of us), go find an auto awesomer and don't go afk. Idle DJ's will be escorted from the stage.";
var modCommands = "cmd's: precede with a '/' or '?' - awesome/dance/bonus, lame/sucks, lettergame, wordgame, fan, skip, snag/nom, quote, info, rules, idle, mods, and help.";
var roomInfo = "The Coding Soundtrack is music for geeks and hackers: for rules visit http://tinyurl.com/codingSoundtrack to follow the room visit http://www.facebook.com/CodingSoundtrack";
var roomHelp = "COMMANDS: ?count - current DJ's song count, ?idle - current DJ's idle times. ?quote - random movie quote, ?awesome - give the current DJ an extra awesome from me.";
var roomUrl = 'http://turntable.fm/coding_soundtrack3';

// game variables
var curSong = "";
var curLast = "";
var curFirst = "";
var gameType = "none";
var curStreak = [];
var curWords = [];
var longStreak = 0;

// bot speech arrays
var quotes = require('./quotes');
var magicWord = "Supercalifragilisticexpialidocious";
var deny = [', that command is restricted.', ', only mods may do that.'];
var haters = [
            "http://img.gawkerassets.com/img/17wl92trr3f0cgif/cmt-medium.gif",
            "http://codermonkey.e-nanigans.com/gifs/haters.gif"
            ];
var meow = [
            "http://imageshack.us/a/img14/1239/psytrance.gif",
            "http://i.imgur.com/xgPJV.gif"
            ];

// ***START GLOBAL CODE HERE***

// global variables
var idleLimit = 15 * 60000;
var waitDJs = 2;
var waitTime = 60000;
var DJcache = [];
var oldDJcache = [];
var activeDJs = [];
var djQueue = [];
var onDecks = 0;
var mods = [];
var modNames = [];
var curdjID = "";
var curdjName = "";
var word = 0;
var bobbing = 0;
var checkId = "";
var setTimer;

// start out with values in case someone asks right when it joins
var count = new Array("x", "x", "x", "x", "x");
var seatOpen = 0;

// start the bot
var bot = new Bot(AUTH, USERID, ROOMID);
bot.debug = true;

// initialize stat tracker
// var roomStatTracker = new StatTracker(bot, "mongodb://skitty:sk1ttyst4ts@linus.mongohq.com:10065/tt-db");

// initialize twitter
var oa = new OAuth("https://api.twitter.com/oauth/request_token",
"https://api.twitter.com/oauth/access_token",
consumer_key,
consumer_secret,
"1.0",
null,
"HMAC-SHA1");

function updateCount() {

  // Update room count
  for (var i = 0; i < 5; i++) {
    if (typeof activeDJs[i] != 'undefined') {
      for (var j in DJcache) {
        if (activeDJs[i] == DJcache[j].id) {
          count[i] = DJcache[j].count;
          //console.log('DJ ' + i + ' has played ' + count[i] + ' songs.');
        }
      }
    } else {
      count[i] = 'x';
      //console.log('DJ ' + i + ' has played ' + count[i] + ' songs.');
    }
  }
}

// force activeDJs change since updateDJs() is too slow to update from live info in some cases
function deleteActiveDJ(djID) {
  for (var j = 0; j < activeDJs.length; j++) {
    if (activeDJs[j] == djID) {
      activeDJs.splice(j, 1);
      j--;
    }
  }
  updateCount();
}

function updateDJs(data) {

  var found = 0;
  var count = 0;
  var olddj = 0;
  var nodj = 0;
  var name = "";
  var date = new Date();
  var z = 999;
  activeDJs = data.room.metadata.djs;

  if (activeDJs.indexOf(USERID) !== -1) {
    onDecks = 1;
  } else {
    onDecks = 0;
  }

  // reset mod caches
  mods = data.room.metadata.moderator_id;
  modNames.length = 0;
  curdjID = data.room.metadata.current_dj;
  curdjName = data.room.metadata.current_song.djname;

  // delete DJs from cache who are no longer DJing
  for (var i = 0; i < DJcache.length; i++) {
    if (data.room.metadata.djs.indexOf(DJcache[i].id) == -1) {
      //console.log('No match found, deleted ' + DJcache[i].name + ' from DJcache.');

      // store a copy in oldDJCache for continued sets
      if ((DJcache[i].count < 3) && (DJcache[i].warn == 0)) {
        oldDJcache.push({
          "id" : DJcache[i].id,
          "count" : DJcache[i].count,
          "name" : DJcache[i].name
        });
      }
      DJcache.splice(i, 1);
      i--;
    }
  }

  // Add DJs to cache that are not present
  for (var i in data.room.metadata.djs) {
    found = 0;
    for (var j in DJcache) {
      checkId = data.room.metadata.djs[i];
      if (data.room.metadata.djs[i] == DJcache[j].id) {
        //console.log('Found a match, do not need to add DJ.');
        found = 1;
        z = 0;
      }
    }

    if (found == 0) {
      // is this an old DJ?
      for (var j = 0; j < oldDJcache.length; j++) {
        if (data.room.metadata.djs[i] == oldDJcache[j].id) {
          count = oldDJcache[j].count;
          oldDJcache.splice(j, 1);
          j--;
          olddj = 1;
        }
      }

      if (nodj != 1) {
        // Find this DJ's name from the room user list
        for (var j in data.users) {
          if (data.users[j].userid == data.room.metadata.djs[i]) {
            name = data.users[j].name;
          }
        }

        DJcache.push({
          "id" : data.room.metadata.djs[i],
          "count" : count,
          "name" : name,
          "date" : date.getTime(),
          "warn" : 0
        });
        z = DJcache.length - 1;

        if (olddj == 1) {
          bot.speak("Welcome back @" + name + "!  You have " + (3 - count) + " song(s) remaining in your set.");
        } else {
          if (djQueue.length > 0) {
            if ( DJcache[z].id == djQueue[0].id) {
              moveQ();
              if ((gameType != "none") && (curLast !== "")) {
                bot.speak(name + ', we are playing the Connection Game!! The next song needs to start with the ' + gameType + ' "' + curLast + '" to keep it going.');
              }
            } else {
              bot.speak('Sorry, @' + name + ', you are jumping the line, please step down.');
              bot.remDj(name);
              DJcache[z].warn = 1;
              seatOpen = 0;
            }
          } else {
            if ((gameType != "none") && (curLast !== "")) {
              switch(gameType) {
                case "letter":
                  bot.speak('We\'re playing the Letter Game. The next song needs to start with "' + curLast + '"');
                  break;

                case "word":
                  bot.speak('We\'re playing the Word Game. The next song needs to contain one of the following words: ' + curWords.join(', '));
                  break;
                }
              }
            }
          }
          //console.log('Added ' + DJcache[DJcache.length - 1].name + ' to DJcache.');
        }
      }
    }

    updateCount();
    var inRoom = 0;
    for (var j in data.users) {
      if (mods.indexOf(data.users[j].userid) != -1) {
        modNames.push(data.users[j].name);
        //console.log(data.users[j].name + ' added to mod list');
      }
      if(djQueue.length >= 1) {
        if(data.users[j].userid == djQueue[0].id) {
          inRoom = 1;
          //console.log(data.users[j].name + ' was found in the room');
        }
      }
    }

    // track when there are less than 5 DJs to let people hop back up
    if (activeDJs.length < 5) {
      if (seatOpen == 0) {
        seatOpen = date;
        //console.log('Less than 5 DJs present, seatOpen set to ' + seatOpen);
      }

      // if there are NO DJs, don't wait to jump on the table
      if (activeDJs.length == 0) {
        getUp();
      }

      if(activeDJs.length >= 3) {
        getDown();
      }

      // Check to see if on-deck DJ is in the room
      if (djQueue.length >= 1) {
        // If the DJ has left the room, skip to the next DJ without waiting.
        if ((inRoom == 0) && ( seatOpen != 0)) {
          if(djQueue[0].bump == 0) {
            bot.speak(djQueue[0].name + ' not found in the room.');
            djQueue[0].bump = 1;
            skip();
          } else {
            bot.speak(djQueue[0].name + ' is still not in the room and is being dropped from the queue.');
            moveQ();
          }
          seatOpen = date;
        } else {
          // If there is a queue and the on deck DJ has not stepped up, skip to the next DJ.
          if (((date - seatOpen) > waitTime) && (seatOpen !== 0)) {
            if(djQueue[0].bump == 0) {
              djQueue[0].bump = 1;
              skip();
            } else {
              bot.speak(djQueue[0].name + ' has not stepped up a second time and is being dropped from the queue.');
              moveQ();
            }
            seatOpen = date;
          }
        }
      }
      nextDJ();
    } else {
      seatOpen = 0;
      //console.log('DJ table is full, seatOpen set to ' + seatOpen);
    }
  }


  function sayCount(user) {

    // direct the count at a user or at the room
    if (typeof user !== 'undefined') {
      bot.speak(user + ': count is ' + count[0] + '-' + count[1] + '-' + count[2] + '-' + count[3] + '-' + count[4]);
    } else {
      bot.speak('Count: ' + count[0] + '-' + count[1] + '-' + count[2] + '-' + count[3] + '-' + count[4]);
    }
    //console.log('Count: ' + count[0] + '-' + count[1] + '-' + count[2] + '-' + count[3] + '-' + count[4]);
  }

  function updateIdle(user) {
    var date = new Date();

    for (var i in DJcache) {
      if (user == DJcache[i].id) {
        DJcache[i].date = date.getTime();
        //console.log('updated ' + DJcache[i].name + ' date to ' + DJcache[i].date);
      }
    }
  }

  function nextDJ() {
    if (activeDJs.length <= 1) {
      getUp();
    }
    if (activeDJs.length > 4) {
      bot.remDj(USERID);
    }
    if (djQueue.length > 0) {
      if (activeDJs.length <= 4) {
        bot.speak('@' + djQueue[0].name + ' It\'s your turn, please step up to DJ!');
      } else {
        bot.speak(djQueue[0].name + ' is now on deck.');
        getDown();
      }
    } else {
      //bot.speak('DJ Queue empty - All open seats are fair game.');
      oldDJcache.length = 0;
    }
  }

  function moveQ() {
    //console.log('starting moveQ() - djQueue.length is ' + djQueue.length + '.');
    if (djQueue.length > 0) {
      //console.log('Deleting ' + djQueue[0].name + ' from djQueue');
      djQueue.splice(0,1);
      seatOpen = 0;
    }
  }

  function skip() {
    //console.log('starting skip() - djQueue.length is ' + djQueue.length + '.');
    if (djQueue.length >= 2) {
      var tempQ = [];
      bot.speak('Moving ' + djQueue[0].name + ' down in the queue');
      tempQ[0] = djQueue[1];
      tempQ[1] = djQueue[0];
      djQueue[0] = tempQ[0];
      djQueue[1] = tempQ[1];
      seatOpen = 0;
    } else {
      bot.speak('No one else in line, removing ' + djQueue[0].name + ' from the queue');
      moveQ();
    }
  }

  function bump(usr) {
    if (typeof usr !== 'undefined') {
      //console.log('starting bump() - djQueue.length is ' + djQueue.length + '.');
      if ((djQueue.length >= 2 ) && (usr !== 0)) {
        var bumper = [];
        //console.log('bumping ' + djQueue[usr].name + ' down in the djQueue');
        bumper[0] = djQueue[usr];
        djQueue.splice(usr, 1);
        djQueue.unshift(bumper[0]);
        seatOpen = 0;
      } else {
        bot.speak(djQueue[usr].name + ' is already at the top of the queue.');
      }
    } else {
      bot.speak('No one to bump.');
    }
  }

  function getUp() {
    if (onDecks == 0) {
      bot.addDj();
      onDecks = 1;
    }
  }

  function getDown() {
    //if (onDecks == 1) {
      bot.remDj(USERID);
      onDecks = 0;
    //}
  }

  function snagSong() {
    bot.roomInfo(function(data) {
      if (data.room.metadata.current_song._id) {
        bot.playlistAdd(data.room.metadata.current_song._id, function(data) {
          bot.playlistReorder(0, -1);
          if (data.success) {
            bot.speak('Song added to my queue.');
          } else {
            bot.speak('Failed for some reason.');
          }
        });
      } else {
        bot.speak('Unable to add song to queue.');
      }
    });
  }

  function game(song, dj) {
    var gLast = "";
    var oldLast = curLast;
    var gamePass = "";

    // remove special characters from the song name
    var sl = song.indexOf('(');
    if (sl != -1) {
      song = song.slice(0, sl);
    }
    song = song.replace(/[^a-zA-Z 0-9]+/g,'');

    // find match
    switch(gameType) {
      case "word":
        curWords = song.split(' ');
        var lastWords = oldLast.split(' ');
        for (var sf in curWords) {
          if (lastWords.indexOf(curWords[sf]) != -1) {
            gamePass = "OK";
            //console.log ('found ' + curWords[sf]);
          }
        }
        curLast = song;
        if (gamePass == "OK") {
          curStreak.push(song);
          bot.speak( dj + ' has kept it going!! Current streak is at ' + curStreak.length + ' songs. The next song needs to contain one of the following words: ' + curWords.join(', '));
        }
        break;

      case "letter":
        sl = song.length - 1;
        curLast = song.charAt(sl);
        curFirst = song.charAt(0);
        if (curFirst == oldLast) {
          gamePass = "OK";
          curStreak.push(song);
          bot.speak( dj + ' has kept it going!! Current streak is at ' + curStreak.length + ' songs. The next song needs to start with "' + curLast + '" to keep it going.');
        }
        break;

      case "die":
        gamePass = "";
        break;
    }
    
    // Return results
    if (oldLast !== "") {
      if (gamePass != "OK") {
        bot.speak (dj + ' has broken the streak. The last ' + gameType + ' game is over.');
        if (curStreak.length > longStreak) {
          longStreak = curStreak.length;
          bot.speak ('***NEW RECORD: ' + longStreak + ' SONGS***');
        } else {
          bot.speak ('FINAL SCORE: ' + curStreak.length + ', the current record is ' + longStreak + ' songs.');
        }
        curStreak.length = 0;
        gameType = "none";
        curLast = "";
        curWords = "";
        //console.log('Ending ' + gameType + ' connection game');
      }
    } else {
      switch(gameType) {
        case "letter":
          bot.speak('We\'re playing the Letter Game. The next song needs to start with "' + curLast + '"');
          break;

        case "word":
          bot.speak('We\'re playing the Word Game. The next song needs to contain one of the following words: ' + curWords.join(', '));
          break;
      }
    }
  }

  // action triggers
  bot.on('roomChanged', function (data) {
    // Reset the users list
    theUsersList = { };

    var users = data.users;
    for (var i=0; i<users.length; i++) {
      var user = users[i];
      theUsersList[user.userid] = user;
    }
  });

  bot.on('rem_dj', function (data) {
    bot.roomInfo(function (data) { updateDJs(data); });
  });

  //bot.on('registered', function(data) {
    //var djName = data.user[0];
    //console.log("ONDECK ONDECK ONDECK" + seatOpen);
    //if(mods.indexOf(data.userid) == false) {
      //bot.pm("Welcome to the CS Lounge.", data.user[0].userid);
    //} else {
      //bot.pm("You shouldn't be getting this.", data.user[0].userid);
    //}
  //});

  bot.on('add_dj', function (data) {
    bot.roomInfo( function (data) { updateDJs(data); });
  });

  bot.on('newsong', function (data) {
    var rmQueue = [];
    var date = new Date();
    var curdate = date.getTime();
    var uri = "";
    var curdj = "";
    var curSong;

    //reset bobbing
    bobbing = 0;

    clearTimeout(setTimer);

    // shouldn't need this, but will ensure we don't get out of sync
    bot.roomInfo(function (data) { updateDJs(data); });
    curSong = data.room.metadata.current_song.metadata.song.toUpperCase();
    curdjID =  data.room.metadata.current_dj;
    curdjName = data.room.metadata.current_song.djname;

    //if(warning === 1) {
      //bot.speak('..And boom goes the dynamite.');
      //rmQueue.push(DJcache[i].id);
      //deleteActiveDJ(DJcache[i].id);
      //warning = 0;
    //}

    // Increase count for active DJ, remove DJs over limit, and remove idle DJs
    for (var i in DJcache) {
      if (DJcache[i].id == data.room.metadata.current_dj) {
        DJcache[i].count++;
        curdj = DJcache[i].name;
        //console.log(curdj + ' has NOW played ' + DJcache[i].count + ' songs.');
        updateCount();
      } else {
        if ((DJcache[i].count >= 4) && (djQueue.length > 0) && (activeDJs.length == 5)) {
          //bot.speak("Thanks for the music, @" + DJcache[i].name + "!  It's @" + djQueue[0].name + "'s turn.");
          bot.speak("@" + DJcache[i].name + " we've got peeps waiting to DJ and you've hit the song limit. So I'm removing you to keep things flowing. No hard feelings? :smile:");
          rmQueue.push(DJcache[i].id);
          deleteActiveDJ(DJcache[i].id);
        }
      }
      // check for idle enforcement,
      // must check activeDJs since it may have been modified above and differs from DJcache now
      if (activeDJs.length == 5) {
        // remove the DJ if they are idle and it's been at least a minute since the warning
        if (((curdate - DJcache[i].date) > idleLimit) && (DJcache[i].idlewarn !== 0) && ((curdate - DJcache[i].idlewarn) > 60000)) {
          bot.speak("@" + DJcache[i].name + " you have been removed for idle DJ'ing.");
          rmQueue.push(DJcache[i].id);
          deleteActiveDJ(DJcache[i].id);
        }

        // warn DJs about idle time
        if ((( curdate - DJcache[i].date) > idleLimit) && (DJcache[i].idlewarn == 0)) {
          bot.speak('@' + DJcache[i].name + ' you are idle more than ' + ( idleLimit / 1000 / 60 ) + ' minutes.  You will lose your seat at the end of this song if you are still idle.');
          DJcache[i].idlewarn = curdate;
        }

        // if DJs wake up, remove the warning bit so they don't get removed without warning later
        if ((( curdate - DJcache[i].date ) < idleLimit ) && (DJcache[i].idlewarn !== 0 )) {
          DJcache[i].idlewarn = 0;
        }
      }
    }
    // are we playing a game?
    if (gameType != "none") {
      game(curSong, curdj);
    }
    // do any DJs need to be removed?
    if (rmQueue.length > 0) {
      for (var k in rmQueue) {
        bot.remDj(rmQueue[k]);
      }
    }

    // tweet current song
    uri = data.room.metadata.current_song.metadata.artist + ' - ' + data.room.metadata.current_song.metadata.song;

    if (((uri.length + curdj.length) > 74) && ((uri.length + curdj.length) < 87)) {
      uri = curdj + ' is now playing ' + uri + roomUrl;
    } else {
      if ((uri.length + curdj.length) > 87) {
        uri = curdj + ' is now playing ' + uri.substr(0,122);
      } else {
        uri = curdj + ' is now playing ' + uri + ' #turntablefm ' + roomUrl;
      }
    }

    uri = "https://api.twitter.com/1/statuses/update.json?status=" + encodeURIComponent(uri);

    oa.getProtectedResource(
      uri, "POST", access_token, access_token_secret, function(data) {
        //console.log("Twitter returned: " + data);
      }
    );
  });

  // All basic commands run through Private Messaging
  bot.on('pmmed', function (data) {
    var date = new Date();
    var idle = 0;
    var cmd = "";
    var o = Math.floor(Math.random() * deny.length);

    if (activeDJs.indexOf(data.userid) != -1) {
      updateIdle(data.userid);
    }

    if (data.text.charAt(0) == "?" || data.text.charAt(0) == "/") {
      var sp = data.text.search(" ");
      if (sp != -1) {
        cmd = data.text.slice(1, sp);
      } else {
        cmd = data.text.slice(1);
      }
      switch (cmd) {
        // COUNT command
        case "count":
          sayCount(data.name);
          //console.log(data.name + ' triggered count keyword.');
          break;

        // REALCOUNT command
        case "realcount":
          if (theUsersList[data.senderid].name === 'ZzaichikK') {
            if (data.text.match(/^\?realcount [0-9x]+-[0-9x]+-[0-9x]+-[0-9x]+-[0-9x]+$/)) {
              var newcount = data.text;
              newcount = newcount.replace(/^\?realcount /, "");
              bot.roomInfo(function (data) { updateDJs(data); });
              var tmpcount = newcount.split("-");

              for (var k = 0; k < 5; k++) {
                //console.log('correcting DJ count with activeDJs ' + activeDJs[k]);
                if (typeof activeDJs[k] != 'undefined') {
                  for (var j in DJcache) {
                    if (activeDJs[k] == DJcache[j].id) {
                      DJcache[j].count = tmpcount[k];
                      //console.log('DJ ' + k + ' updated to' + tmpcount[k] + ' songs.');
                    }
                  }
                }
              }

              updateCount();
              bot.pm("Count Updated.", data.senderid);
              sayCount();
            } else {
              bot.pm("Proper syntax is: ?realcount #-#-#-#-# (Use 'x' for empty seats.)", data.senderid);
            }
          } else {
            bot.pm(deny[o], data.senderid);
          }
          break;

          // QUOTE command
          case "quote":
            var q = Math.round(Math.random()*(quotes.length-1));
            bot.pm(quotes[q], data.senderid);
            break;

          // INFO command
          case "info":
          bot.pm(roomInfo, data.senderid);
          break;

        // RULES command
        case "rules":
          bot.pm(roomRules, data.senderid);
          break;

        // IDLE command
        case "idle":
          var idleString = [];
          for (var i in activeDJs) {
            for (var j in DJcache) {
              if (activeDJs[i] == DJcache[j].id) {
                idle = (date - DJcache[j].date) / 1000 / 60;
                idleString = (idleString + "@" + DJcache[j].name + " - " + idle.toFixed(0) + "min. || ");
              }
            }
          }
          bot.pm(idleString, data.senderid);
          break;

        // AWESOME command - awesome a song
        case "awesome":
        case "dance":
        case "bonus":
          var rand_quote = quotes.length;
          var q = Math.floor(Math.random() * rand_quote);
          bot.speak(quotes[q]);

          // Check to see if it's the current DJ
          //if(data.senderid == curdjID) {
            //bot.pm("You can't awesome yourself ..I will let you lame yourself though.", data.senderid);
          //} else {
            if (bobbing == 0) {
              bot.bop();
              bobbing = 1;
              bot.pm("Bonus awesome given", data.senderid);
            } else {
              //bot.speak(data.name + ', I can\'t bop my head any faster than this!');
              bot.pm("Bonus already used.", data.senderid);
            }
          //}
          break;

        // warning current day to skip
        case "warn":
          bot.speak("@" + curdjName + " - The song you're playing is not within room format. Please skip in the next 20 seconds or you will be escorted form the stage.");
          setTimer = setTimeout(function() {
            bot.remDj(curdjID);
          }, 20000);
          break;

        case "unwarn":
          bot.speak("@" + curdjName + " - Never mind, your track is okay, you don't have to skip.");
          clearTimeout(setTimer);
          break;

        case "lame":
        case "sucks":
          var rand_quote = quotes.length;
          var q = Math.floor(Math.random() * rand_quote);
          bot.speak(quotes[q]);
          bot.vote('down');
          bot.pm("Song lamed " + theUsersList[data.senderid].name, data.senderid + ".");
          break;

        //letter game command - rules
        case "lettergame":
        case "letterGame":
        case "letter game":
          bot.pm("The first letter of each song must start with the last letter of the previous tune.", data.senderid);
          break;

        // word game command - rules
        case "wordgame":
        case "wordGame":
        case "word game":
          bot.pm("The title of each song must contain at least one word from the title of the previous tune.", data.senderid);
          break;

        //FAN command - become fan of current dj, make the bot a fan
        case "fan":
          bot.becomeFan();
          bot.pm("I am now your fan! Hi Five buddy.", data.senderid);
          break;

        //SKIP command skip the bots current song, only Mods
        case "skip":
          if (mods.indexOf(data.senderid) != -1) {
            bot.skip();
          } else {
            bot.pm(deny[o], data.senderid);
          }
          break;

        // SNAG current song and add to bots queue
        case "snag":
        case "nom":
          if (mods.indexOf(data.userid) != -1) {
            snagSong();
          } else {
            bot.pm(deny[o], data.senderid);
          }
          break;

        case "command":
        case "commands":
        case "cmd":
        case "cmds":
          bot.pm(modCommands, data.senderid);
          break;

        // LIST command
        case "list":
        case "queue":
          if (djQueue.length > 0) {
            var tlist = [];
            for (var n = 0; n < djQueue.length; n++) {
              tlist[n] = ' ' + djQueue[n].name;
            }
            bot.pm("DJ Queue: " + tlist.toString(), data.senderid);
          } else {
            bot.pm("DJ Queue is currently empty.", data.senderid);
          }
          //console.log( data.name + " triggered queue keyword.");\
          break;

        // MODS command
        case "mods":
        case "moderators":
          bot.pm("Current Moderators: " + modNames.join(', '), data.senderid);
          break;

        // HELP command
        case "help":
          bot.pm(roomHelp, data.senderid);
          break;
      }
    } else {
      if (data.text.match(magicWord)) {
        if (word == 0) {
          bot.speak("Oh, SHIIIT!!!!");
          word = 1;
        } else {
          bot.speak("Oh, SHIIIT!!!!");
          word = 0;
        }
      }
    }
  });

  // Some of the more Generic Commands can still be accessed through the basic chat window
  bot.on('speak', function (data) {
    var date = new Date();
    var idle = 0;
    var cmd = "";
    var o = Math.floor(Math.random() * deny.length);

    if (activeDJs.indexOf(data.userid) != -1) {
      updateIdle(data.userid);
    }

    // Respond to "/fuck you" phrase
    if (data.text.match(/fuck you s?kitty/)) {
      bot.speak('Fuck You too, @'+data.name);
    }

    if (data.text.match(/^exactly/)) {
      bot.speak('EXACTLY!!');
    }

    if (data.text.charAt(0) == "/" || data.text.charAt(0) == "?") {
      var sp = data.text.search(" ");
      if (sp != -1) {
        cmd = data.text.slice(1, sp);
      } else {
        cmd = data.text.slice(1);
      }
      switch (cmd) {
        case "count":
          sayCount(data.name);
          //console.log(data.name + ' triggered count keyword.');
          break;
        // GAME command
        case "game":
          if (data.text.match(/^\?game .+$/)) {
            if (gameType == "none") {
              var game = data.text;
              game = game.replace(/^\?game /, "");
              if ((game == "letter") || (game == "word")) {
                gameType = game;
                bot.speak("We'll start playing the " + gameType + " game with the next song.");
              } else {
                bot.speak("?game word OR ?game letter, those are your choices.");
              }
            } else {
              bot.speak("We're already playing the " + gameType + " game.");
            }
          } else {
            switch(gameType) {
              case "letter":
                bot.speak("We're playing the Letter Game. The first letter of each song must start with the last letter of the previous tune.");
                break;

              case "word":
                bot.speak("We're playing the Word Game. The title of each song must contain at least one word from the title of the previous tune.");
                break;

              case "none":
                bot.speak("We're currently not playing a game. Say '?game word' or '?game letter' to start one.");
                break;
            }
          }
          break;

        case "event":
          bot.speak("There's 2 events right now! 1.DJ Will Kill's 10k party starts at 5:00pm EST -and- 2.Variety's 50k party starts at 8:30pm EST! Both will be awesome, they're like all our other parties; It's FFA, 1 play and down, wait 1 track to click back up. Completely open format (no genre resitrictions)!! Be here.");
          break;

        // QUOTE command
        case "quote":
          var q = Math.round(Math.random()*(quotes.length-1));
          bot.speak(quotes[q]);
          break;

        // INFO command
        case "info":
          bot.speak(roomInfo);
          break;

        // RULES command
        case "rules":
          bot.speak(roomRules);
          break;

        case "djrules":
          bot.speak(djRules);
          break;

        case "theme":
          bot.speak("Funky Fuckin' Friday! Every Friday we implement a theme: Electro Soul, Glitch, Funk, Hip Hop, medium Dubstep(if it has some funk to it) ...as long as it has a sick beat and some funk you're winning.");
          break;

        // letter game command - rules
        case "lettergame":
        case "letterGame":
        case "letter game":
          bot.speak("The first letter of each song must start with the last letter of the previous tune.");
          break;

        // word game command - rules
        case "wordgame":
        case "wordGame":
        case "word game":
          bot.speak("The title of each song must contain at least one word from the title of the previous tune.");
          break;

        // IDLE command
        case "idle":
          var idleString = [];
          for (var i in activeDJs) {
            for (var j in DJcache) {
              if (activeDJs[i] == DJcache[j].id) {
                idle = (date - DJcache[j].date) / 1000 / 60;
                idleString = (idleString + "@" + DJcache[j].name + " - " + idle.toFixed(0) + "min. || ");
              }
            }
          }
          bot.speak(idleString);
          break;

        // LIST command
        case "list":
        case "queue":
          if (djQueue.length > 0) {
            var tlist = [];
            for (var n = 0; n < djQueue.length; n++) {
              tlist[n] = ' ' + djQueue[n].name;
            }
            bot.speak("DJ Queue:" + tlist.toString());
          } else {
            bot.speak("DJ Queue is currently empty.");
          }
          //console.log( data.name + ' triggered queue keyword.');
          break;

        // AWESOME command - awesome a song
        case "awesome":
        case "dance":
        case "bonus":
        case "boogie":
        case "dope":
          // Check to see if it's the current DJ
          //if(data.userid === curdjID) {
            //bot.speak("@" + data.name + ", can't awesome yourself buddy, that's cheating. I will let you lame yourself though.");
          //} else {
            if (bobbing === 0) {
              bot.bop();
              bobbing = 1;
              bot.speak("Bonus. :thumbsup:");
              //bot.speak("@" + curdjName + " - a bonus Awesome from " + data.name + "! Good pick.");
            } else {
              bot.speak("Bonuses to the max! Good play youse. :thumbsup:");
            }
          //}
          break;

        case "meow":
          var mm = Math.round(Math.random()*(meow.length-1));
          bot.speak(meow[mm]);
          break;

        case "bop":
          bot.speak("http://e-nanigans.com/pics/yunobop.jpg");
          break;

        // MODS command
        case "mods":
        case "moderators":
          bot.speak("Current Moderators: " + modNames.join(", "));
          break;

        // HELP command
        case "help":
          bot.speak(roomHelp);
          break;

        case "command":
        case "commands":
          bot.speak(modCommands);
          break;

        case "trance":
        case "trippy":
          bot.speak("http://25.media.tumblr.com/tumblr_lqbfmxB5bD1qf4smjo1_500.gif");
          break;

        case "haters":
          var h = Math.round(Math.random()*(haters.length-1));
          bot.speak(haters[h]);
          break;

        case "flow":
          bot.speak("http://e-nanigans.com/pics/yunoflow.jpg");
          break;

        case "escortme":
          bot.remDj(data.userid);
          break;

        //to to items
        //case "cb":
          //bot.speak("to do.");
          //break;

        //case "dp":
          //case "donkeypunch":
          //bot.speak("to do.");
          //break;
      }
    } else {
      if (data.text.match('http://turntable.fm')) {
        bot.speak("@" + data.name + " is a dirty, dirty spammer.");
      } else {
        if (data.text.match(magicWord)) {
          if (word == 0) {
            bot.speak("AAAAAAAAAAHHHHHHHH!!!!");
            word = 1;
          } else {
            bot.speak("AAAAAAAAAHHHHHHHHH!!!!");
            word = 0;
          }
        }
      }
    }
  });

  bot.on('update_votes', function (data) {
    for (var i in data.room.metadata.votelog) {
      if (activeDJs.indexOf(data.room.metadata.votelog[i][0]) != -1 ) {
        updateIdle(data.room.metadata.votelog[i][0]);
      }
    }
  });

  bot.on('roomChanged',  function (data) {
    //console.log('The bot has changed rooms.');
    bot.roomInfo( function (data) { updateDJs(data);
  });
});

// END