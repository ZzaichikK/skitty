// Some of the more Generic Commands can still be accessed through the basic chat window
bot.on('speak', function (data) {
  var date = new Date();
  var idle = 0;
  var cmd = "";
  var o = Math.floor(Math.random() * deny.length);

  if (activeDJs.indexOf(data.userid) != -1) {
    updateIdle(data.userid);
  }

  // Respond to "/fuck you" phrase
  if (data.text.match(/fuck you s?kitty/)) {
    bot.speak('Fuck You too, @'+data.name);
  }

  if (data.text.match(/^exactly/)) {
    bot.speak('EXACTLY!!');
  }

  if (data.text.charAt(0) == "/" || data.text.charAt(0) == "?") {
    var sp = data.text.search(" ");
    if (sp != -1) {
      cmd = data.text.slice(1, sp);
    } else {
      cmd = data.text.slice(1);
    }
    switch (cmd) {
      case "count":
        sayCount(data.name);
        //console.log(data.name + ' triggered count keyword.');
      break;
      // GAME command
      case "game":
        if (data.text.match(/^\?game .+$/)) {
          if (gameType == "none") {
            var game = data.text;
            game = game.replace(/^\?game /, "");
            if ((game == "letter") || (game == "word")) {
              gameType = game;
              bot.speak("We'll start playing the " + gameType + " game with the next song.");
            } else {
              bot.speak("?game word OR ?game letter, those are your choices.");
            }
          } else {
            bot.speak("We're already playing the " + gameType + " game.");
          }
        } else {
          switch(gameType) {
            case "letter":
              bot.speak("We're playing the Letter Game. The first letter of each song must start with the last letter of the previous tune.");
            break;

            case "word":
              bot.speak("We're playing the Word Game. The title of each song must contain at least one word from the title of the previous tune.");
            break;

            case "none":
              bot.speak("We're currently not playing a game. Say '?game word' or '?game letter' to start one.");
            break;
          }
        }
      break;

      case "event":
        bot.speak("There's 2 events right now! 1.DJ Will Kill's 10k party starts at 5:00pm EST -and- 2.Variety's 50k party starts at 8:30pm EST! Both will be awesome, they're like all our other parties; It's FFA, 1 play and down, wait 1 track to click back up. Completely open format (no genre resitrictions)!! Be here.");
      break;

      // QUOTE command
      case "quote":
        var q = Math.round(Math.random()*(quotes.length-1));
        bot.speak(quotes[q]);
      break;

      // INFO command
      case "info":
        bot.speak(roomInfo);
      break;

      // RULES command
      case "rules":
        bot.speak(roomRules);
      break;

      case "djrules":
        bot.speak(djRules);
      break;

      case "theme":
        bot.speak("Funky Jams Friday! Every Friday we implement a theme: Electro Soul, Glitch, Funk, Hip Hop, medium Dubstep(if it has some funk to it) ...as long as it has a sick beat and some funk you're winning.");
      break;

      // letter game command - rules
      case "lettergame":
      case "letterGame":
      case "letter game":
        bot.speak("The first letter of each song must start with the last letter of the previous tune.");
      break;

      // word game command - rules
      case "wordgame":
      case "wordGame":
      case "word game":
        bot.speak("The title of each song must contain at least one word from the title of the previous tune.");
      break;

      // IDLE command
      case "idle":
        var idleString = [];
        for (var i in activeDJs) {
          for (var j in DJcache) {
            if (activeDJs[i] == DJcache[j].id) {
              idle = (date - DJcache[j].date) / 1000 / 60;
              idleString = (idleString + "@" + DJcache[j].name + " - " + idle.toFixed(0) + "min. || ");
            }
          }
        }
        bot.speak(idleString);
      break;

      // LIST command
      case "list":
      case "queue":
        if (djQueue.length > 0) {
          var tlist = [];
          for (var n = 0; n < djQueue.length; n++) {
            tlist[n] = ' ' + djQueue[n].name;
          }
          bot.speak("DJ Queue:" + tlist.toString());
        } else {
          bot.speak("DJ Queue is currently empty.");
        }
        //console.log( data.name + ' triggered queue keyword.');
      break;

      // AWESOME command - awesome a song
      case "awesome":
      case "dance":
      case "bonus":
      case "boogie":
        // Check to see if it's the current DJ
        //if(data.userid === curdjID) {
        //  bot.speak("@" + data.name + ", can't awesome yourself buddy, that's cheating. I will let you lame yourself though.");
        //} else {
          if (bobbing === 0) {
            bot.bop();
            bobbing = 1;
            bot.speak("Bonus. :thumbsup:");
            //bot.speak("@" + curdjName + " - a bonus Awesome from " + data.name + "! Good pick.");
          } else {
            bot.speak("Bonuses to the max! Good play youse. :thumbsup:");
          }
        //}
      break;

      case "meow":
        bot.speak("MEOW.  http://i.imgur.com/xgPJV.gif");
      break;

      // MODS command
      case "mods":
      case "moderators":
        bot.speak("Current Moderators: " + modNames.join(", "));
      break;

      // HELP command
      case "help":
        bot.speak(roomHelp);
      break;

      case "command":
      case "commands":
        bot.speak(modCommands);
      break;

      case "trance":
      case "trippy":
        bot.speak("http://25.media.tumblr.com/tumblr_lqbfmxB5bD1qf4smjo1_500.gif");
      break;

      case "haters":
        var h = Math.round(Math.random()*(haters.length-1));
        bot.speak(haters[h]);
      break;

      case "escortme":
        bot.remDj(data.userid);
      break;

      // to to items
      //case "cb":
      //  bot.speak("to do.");
      //break;

      //case "dp":
      //case "donkeypunch":
      //  bot.speak("to do.");
      //break;
    }
  } else {
    if (data.text.match('http://turntable.fm')) {
      bot.speak("@" + data.name + " is a dirty, dirty spammer.");
    } else {
      if (data.text.match(magicWord)) {
        if (word == 0) {
          bot.speak("AAAAAAAAAAHHHHHHHH!!!!");
          word = 1;
        } else {
          bot.speak("AAAAAAAAAHHHHHHHHH!!!!");
          word = 0;
        }
      }
    }
  }
});