// All basic commands run through Private Messaging
bot.on('pmmed', function (data) {
  var date = new Date();
  var idle = 0;
  var cmd = "";
  var o = Math.floor(Math.random() * deny.length);

  if (activeDJs.indexOf(data.userid) != -1) {
    updateIdle(data.userid);
  }
  if (data.text.charAt(0) == "?" || data.text.charAt(0) == "/") {
    var sp = data.text.search(" ");
    if (sp != -1) {
      cmd = data.text.slice(1, sp);
    } else {
      cmd = data.text.slice(1);
    }
    switch (cmd) {
      // COUNT command
      case "count":
        sayCount(data.name);
        //console.log(data.name + ' triggered count keyword.');
      break;

      // REALCOUNT command
      case "realcount":
        if (theUsersList[data.senderid].name === 'ZzaichikK') {
          if (data.text.match(/^\?realcount [0-9x]+-[0-9x]+-[0-9x]+-[0-9x]+-[0-9x]+$/)) {
            var newcount = data.text;
            newcount = newcount.replace(/^\?realcount /, "");
            bot.roomInfo(function (data) { updateDJs(data); });
            var tmpcount = newcount.split("-");

            for (var k = 0; k < 5; k++) {
              //console.log('correcting DJ count with activeDJs ' + activeDJs[k]);
              if (typeof activeDJs[k] != 'undefined') {
                for (var j in DJcache) {
                  if (activeDJs[k] == DJcache[j].id) {
                    DJcache[j].count = tmpcount[k];
                    //console.log('DJ ' + k + ' updated to' + tmpcount[k] + ' songs.');
                  }
                }
              }
            }

            updateCount();
            bot.pm("Count Updated.", data.senderid);
            sayCount();
          } else {
            bot.pm("Proper syntax is: ?realcount #-#-#-#-# (Use 'x' for empty seats.)", data.senderid);
          }
        } else {
          bot.pm(deny[o], data.senderid);
        }
      break;

      // QUOTE command
      case "quote":
        var q = Math.round(Math.random()*(quotes.length-1));
        bot.pm(quotes[q], data.senderid);
      break;

      // INFO command
      case "info":
        bot.pm(roomInfo, data.senderid);
      break;

      // RULES command
      case "rules":
        bot.pm(roomRules, data.senderid);
      break;

      // IDLE command
      case "idle":
        var idleString = [];
        for (var i in activeDJs) {
          for (var j in DJcache) {
            if (activeDJs[i] == DJcache[j].id) {
              idle = (date - DJcache[j].date) / 1000 / 60;
              idleString = (idleString + "@" + DJcache[j].name + " - " + idle.toFixed(0) + "min. || ");
            }
          }
        }
        bot.pm(idleString, data.senderid);
      break;

      // AWESOME command - awesome a song
      case "awesome":
      case "dance":
      case "bonus":
        var rand_quote = quotes.length;
        var q = Math.floor(Math.random() * rand_quote);
        bot.speak(quotes[q]);

        // Check to see if it's the current DJ
        //if(data.senderid == curdjID) {
        //  bot.pm("You can't awesome yourself ..I will let you lame yourself though.", data.senderid);
        //} else {
          if (bobbing == 0) {
            bot.bop();
            bobbing = 1;
            bot.pm("Bonus awesome given", data.senderid);
          } else {
//            bot.speak(data.name + ', I can\'t bop my head any faster than this!');
            bot.pm("Bonus already used.", data.senderid);
          }
        //}
      break;

      // warning current day to skip
      case "warn":
        bot.speak("@" + curdjName + " - The song you're playing is not within room format. Please skip in the next 20 seconds or you will be escorted form the stage.");
        
        setTimer = setTimeout(function() {
          bot.remDj(curdjID);
        }, 20000);
        
      break;

      case "unwarn":
        bot.speak("@" + curdjName + " - Never mind, your track is okay, you don't have to skip.");
        
        clearTimeout(setTimer);

      break;

      case "lame":
      case "sucks":
        var rand_quote = quotes.length;
        var q = Math.floor(Math.random() * rand_quote);
        bot.speak(quotes[q]);

        bot.vote('down');
        bot.pm("Song lamed " + theUsersList[data.senderid].name, data.senderid + ".");
      break;

      // letter game command - rules
      case "lettergame":
      case "letterGame":
      case "letter game":
        bot.pm("The first letter of each song must start with the last letter of the previous tune.", data.senderid);
      break;

      // word game command - rules
      case "wordgame":
      case "wordGame":
      case "word game":
        bot.pm("The title of each song must contain at least one word from the title of the previous tune.", data.senderid);
      break;

      // FAN command - become fan of current dj, make the bot a fan
      case "fan":
        bot.becomeFan();
        bot.pm("I am now your fan! Hi Five buddy.", data.senderid);
      break;

      // SKIP command skip the bots current song, only Mods
      case "skip":
        if (mods.indexOf(data.senderid) != -1) {
          bot.skip();
        } else {
          bot.pm(deny[o], data.senderid);
        }
      break;
        
      // SNAG current song and add to bots queue
      case "snag":
      case "nom":
        if (mods.indexOf(data.userid) != -1) {
          snagSong();
        } else {
          bot.pm(deny[o], data.senderid);
        }
      break;

      case "command":
      case "commands":
      case "cmd":
      case "cmds":
          bot.pm(modCommands, data.senderid);
      break;

      // LIST command
      case "list":
      case "queue":
        if (djQueue.length > 0) {
          var tlist = [];
          for (var n = 0; n < djQueue.length; n++) {
            tlist[n] = ' ' + djQueue[n].name;
          }
          bot.pm("DJ Queue: " + tlist.toString(), data.senderid);
        } else {
          bot.pm("DJ Queue is currently empty.", data.senderid);
        }
        //console.log( data.name + " triggered queue keyword.");
      break;

      // MODS command
      case "mods":
      case "moderators":
        bot.pm("Current Moderators: " + modNames.join(', '), data.senderid);
      break;

      // HELP command
      case "help":
        bot.pm(roomHelp, data.senderid);
      break;
    }
  } else {
    if (data.text.match(magicWord)) {
      if (word == 0) {
        bot.speak("Oh, SHIIIT!!!!");
        word = 1;
      } else {
        bot.speak("Oh, SHIIIT!!!!");
        word = 0;
      }
    }
  }
});